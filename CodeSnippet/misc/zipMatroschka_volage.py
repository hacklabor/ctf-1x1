import bz2
import os
from zipfile import ZipFile
import shutil
import lzma
import gzip

def _overwrite(bin_data, file_path):
    with open("temp.bin", "wb") as file:
        file.write(bin_data)
        file.close()

    shutil.move("temp.bin", file_path)

def bzip2_unzip(file_path):
    with open(file_path, "rb") as file:
        bin_data = bz2.decompress(file.read())
        file.close()

    _overwrite(bin_data, file_path)

def zip_unzip(file_path):
    zf = ZipFile(file_path, 'r')
    zf.extract(member='flag.txt', path="extracted")
    zf.close()

    shutil.move("extracted/flag.txt", "flag")

def xz_unzip(file_path):
    bin_data = lzma.open(file_path).read()

    _overwrite(bin_data, file_path)

def gzip_unzip(file_path):
    with gzip.open(file_path, 'rb') as file:
        bin_data = file.read()

    _overwrite(bin_data, file_path)


def main():
    flag = "flag"
    shutil.copy2("flag.txt", f"{flag}")

    for i in range(2000):
        compress_type = os.popen(f"file {flag}").read().strip()
        print(i, compress_type)
        if "bzip2 compressed" in compress_type:
            bzip2_unzip(flag)
        elif "Zip archive" in compress_type:
            zip_unzip(flag)
        elif "XZ compressed" in compress_type:
            xz_unzip(flag)
        elif "gzip compressed" in compress_type:
            gzip_unzip(flag)
        else:
            print("we got the flag !!")
            print(os.popen(f"cat {flag}"))
            break

main()