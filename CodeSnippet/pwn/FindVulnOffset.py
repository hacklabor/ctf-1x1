

from pwn import *
import sys
import os
    

def find_vuln(p, anzahl):
   
  
    p.sendline(cyclic(anzahl, n=4))
    p.wait()
    core = p.corefile
    print("arch "+core.arch)
    print(hex(core.esp))
    vulnOffset=cyclic_find((str(hex(core.esp))), n=4)
    log.info("VulnOffset :"+ str(vulnOffset))
    file_list=os.listdir("./")
    for fil in file_list:
        
        if fil.find("core")>-1:
            log.info(fil + " deleted")
            os.remove(fil)
            

    return vulnOffset

if __name__ == '__main__':
    
    context.arch='amd64'
    arglen=len(sys.argv)
    if arglen<3 or arglen>4:
        print("Argumente --> 1000 <file>")
        print("1000 = Anzahl der Zeichen; <file> = Filename")
        print("or")
        print("Argumente --> 1000 <server> <port>")
        
        exit(0)
    if arglen==3:
        elf = ELF(str(sys.argv[2]))
        p = elf.process()
    if arglen==4:
        p = remote(str(sys.argv[2]), int(sys.argv[3]))
    
    
    print("VulnOffset :"+ str(find_vuln(p,int(sys.argv[1]))))
    
    


   
